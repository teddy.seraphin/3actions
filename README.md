# 3actions



## Commençons

### Ajoutez des fichiers

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/teddy.seraphin/3actions.git
git branch -M main
git push -uf origin main

### Pour l'action du javascript :

# input

Entre Votre prénom : 
Entrez votre nom : 

# output

Votre nom complet est : 


### Pour l'action du PHP :

# input

<?php
function addNumbers(int $a, int $b) {
  return $a + $b;
}
echo addNumbers(5, "5 days"); 
// puisque strict n'est PAS activé "5 jours" est changé en int(5), et il renverra 10
?>

# output

Le résultat sera 10


### Pour l'action Python :

# input

def my_function(fname):
  print("Bonjour !" + fname)

my_function("Clément")
my_function("Justin")
my_function("Léo")

# output

Le résultat sera Bonjour ! + my_function

Bonjour Clément !
Bonjour Justin !
Bonjour Léo !

## S'intégrer à vos outils

- [ ] [Set up project integrations](https://gitlab.com/teddy.seraphin/3actions/-/settings/integrations)

## Collaborez avec votre équipe

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test et déploiement

Utilisez l'intégration continue intégrée dans GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editer le README

Lorsque vous êtes prêt à faire votre propre README, il vous suffit d'éditer ce fichier et d'utiliser le modèle pratique ci-dessous (ou n'hésitez pas à le structurer comme vous le souhaitez - ce n'est qu'un point de départ !) Merci à [makeareadme.com](https://www.makeareadme.com/) pour ce modèle.

## Suggestions pour un bon README

Chaque projet étant différent, il convient de déterminer quelles sections s'appliquent au vôtre. Les sections utilisées dans le modèle sont des suggestions pour la plupart des projets open source. Gardez également à l'esprit que si un fichier README peut être trop long et détaillé, il vaut mieux être trop long que trop court. Si vous pensez que votre fichier README est trop long, envisagez d'utiliser une autre forme de documentation plutôt que de supprimer des informations.

## Nom

Choisissez un nom explicite pour votre projet.

## Description

Faites savoir aux gens ce que votre projet peut faire concrètement. Fournissez le contexte et ajoutez un lien vers toute référence que les visiteurs pourraient ne pas connaître. Vous pouvez également ajouter ici une liste de caractéristiques ou une sous-section sur le contexte. S'il existe des alternatives à votre projet, c'est un bon endroit pour énumérer les facteurs de différenciation.

## Badges

Sur certains README, vous pouvez voir de petites images qui transmettent des métadonnées, comme le fait que tous les tests du projet passent ou non. Vous pouvez utiliser les boucliers pour en ajouter à votre README. De nombreux services ont également des instructions pour ajouter un badge.

## Visuels

En fonction de ce que vous faites, il peut être judicieux d'inclure des captures d'écran ou même une vidéo (vous verrez souvent des GIFs plutôt que de véritables vidéos). Des outils comme ttygif peuvent vous aider, mais consultez Asciinema pour une méthode plus sophistiquée.

## Installation

Au sein d'un écosystème particulier, il peut y avoir une façon commune d'installer les choses, comme l'utilisation de Yarn, NuGet ou Homebrew. Toutefois, il faut tenir compte du fait que la personne qui lit votre fichier README est un novice et qu'elle a besoin de conseils supplémentaires. L'énumération d'étapes spécifiques permet de lever toute ambiguïté et d'amener les gens à utiliser votre projet aussi rapidement que possible. S'il ne fonctionne que dans un contexte spécifique, comme une version de langage de programmation ou un système d'exploitation particuliers, ou s'il comporte des dépendances qui doivent être installées manuellement, ajoutez également une sous-section Exigences.

## Usage

Utilisez abondamment les exemples et montrez le résultat attendu si vous le pouvez. Il est utile d'avoir en ligne le plus petit exemple d'utilisation que vous pouvez démontrer, tout en fournissant des liens vers des exemples plus sophistiqués s'ils sont trop longs pour être raisonnablement inclus dans la section README.

## Support

Indiquez aux gens où ils peuvent trouver de l'aide. Il peut s'agir de toute combinaison d'un outil de suivi des problèmes, d'un salon de discussion, d'une adresse électronique, etc.

## Roadmap

Si vous avez des idées de communiqués pour l'avenir, il est bon de les répertorier dans la section README.

## Contribution

Indiquez si vous êtes ouvert aux contributions et quelles sont vos conditions pour les accepter.

Pour les personnes qui souhaitent apporter des modifications à votre projet, il est utile de disposer d'une documentation sur la manière de commencer. Il peut s'agir d'un script qu'ils doivent exécuter ou de variables d'environnement qu'ils doivent définir. Rendez ces étapes explicites. Ces instructions pourraient également être utiles à votre futur moi.

Vous pouvez également documenter les commandes permettant de vérifier le code ou d'exécuter des tests. Ces étapes permettent d'assurer la qualité du code et de réduire la probabilité que les changements cassent quelque chose par inadvertance. Il est particulièrement utile de disposer d'instructions pour l'exécution des tests si celle-ci nécessite une configuration externe, comme le démarrage d'un serveur Selenium pour les tests dans un navigateur.

## Auteurs et remerciements

Montrez votre reconnaissance à ceux qui ont contribué au projet.

## License

Pour les projets open source, indiquez comment la licence est accordée.

## Statut du projet

Si vous êtes à court d'énergie ou de temps pour votre projet, ajoutez une note en haut du fichier README indiquant que le développement a été ralenti ou complètement arrêté. Quelqu'un peut choisir de bifurquer votre projet ou de se porter volontaire pour prendre la relève en tant que mainteneur ou propriétaire, permettant ainsi à votre projet de continuer à fonctionner. Vous pouvez également faire une demande explicite pour des mainteneurs.